var TestRadioGroup = cc.Scene.extend({
    onEnter: function () {
        this._super();
        var layout = ccs.load(res.TestTab).node;
        this.addChild(layout);
        layout.setPosition(cc.winSize.width/2,cc.winSize.height/2);

        var btn10 = UIView.create(BMFButton, layout.getChildByName("questTab"));
        btn10.setTitle("10");
        var btn20 = UIView.create(BMFButton, layout.getChildByName("mailTab"));
        btn20.setTitle("20");
        var btn30 = UIView.create(BMFButton, layout.getChildByName("guildTab"));
        btn30.setTitle("30");

        var radioGroup = new RadioGroup();
        radioGroup.add(btn10, 10);
        radioGroup.add(btn20, 20);
        radioGroup.add(btn30, 30);
        radioGroup.setActive(btn10);

        var btn = UIView.load(BMFButton, res.BMFButton);
        btn.setTitle("Get Value");
        btn.addClickEventListener(function (ref) {
            cc.log(radioGroup.getValue());
        });
        this.addChild(btn);

    },
});
