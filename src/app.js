'use strict';

var HelloWorldLayer = cc.Layer.extend({
    sprite:null,
    layout:null,
    button:null,
    callback:null,
    ctor:function () {
        //////////////////////////////
        // 1. super init first
        this._super();
        this.scheduleUpdate();

        /////////////////////////////
        // 2. add a menu item with "X" image, which is clicked to quit the program
        //    you may modify it.
        // ask the window size
        var size = cc.winSize;

        var designLayout = ccs.load(res.WelcomeScene);
        this.layout = designLayout.node;
        if(this.layout != null)
        {
            this.layout.setContentSize(size);
            ccui.helper.doLayout(this.layout);
            this.addChild(this.layout);

            this.button = this.layout.getChildByName("closeBtn");
            this.button.addClickEventListener(function() {
                //this.removeChild(this.layout);
                this.callback();
            }.bind(this));
        }

        return true;
    },
    update: function(dt) {
        this._super(dt);
        //cc.log(this.layout);
    },
    onExit: function() {
        this._super();
        this.unscheduleUpdate();
    },
    setCallback:function(callback) {
        this.callback = callback;
    }
});

var HelloWorldScene = cc.Scene.extend({
    _helloLayer:null,
    _tmp:null,
    _list:[],
    onEnter:function () {
        this._super();

        var layer = new HelloWorldLayer();
        this._helloLayer = layer;

        this.addChild(this._helloLayer);

        layer.setCallback(function() {
            this.removeChild(this._helloLayer);

            var count = 0;
            var clone_interval = setInterval(function()
            {

                if(count %2 === 0) {
                    this._tmp = new HelloWorldLayer();

                    //Create memory leak on Browser
                    this._list.push(this._tmp);

                    this.addChild(this._tmp);
                }
                else {
                    this.removeChild(this._tmp);
                }

                count++;
                if(count > 8000)
                {
                    cc.log("size : " + this._list.length);
                    clearInterval(clone_interval);
                }

            }.bind(this), 1);
        }.bind(this));
    }
});

