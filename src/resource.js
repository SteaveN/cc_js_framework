var res = {
    HelloWorld_png : "res/HelloWorld.png",
    background : "res/ui/bg_demo.jpg",
    WelcomeScene: "res/ui/Layer.json",
    BMFButton: "res/ui/BMFButton.json",
    AnotherBTN: "res/ui/Button.json",
    ContentSwitcher: "res/ui/ContentSwitcher.json",
    UIPopup: "res/ui/UIPopup.json",
    UIDialog: "res/ui/UIDialog.json",
    Toasts: "res/ui/Toasts.json",
    Tab: "res/ui/Tab.json",
    TestTab: "res/ui/TestTab.json",
    ComboBox: "res/ui/ComboBox.json",
    ComboBoxItem: "res/ui/ComboBoxItem.json",
};

var g_resources = [];
for (var i in res) {
    g_resources.push(res[i]);
}
