/**
 * Created by nguyenquangninh on 8/22/16.
 */
var TabGroup = cc.Class.extend({
    _tabList: null,
    _currentTab: null,
    ctor:function () {
        this._tabList = new HashTable();
    },
    addTab:function (tabBtn, callback) {
        this._tabList.set(tabBtn, callback);
        tabBtn.addClickEventListener(function (ref) {
            this.setActive(tabBtn);
        }.bind(this));
    },
    removeTab:function (tabBtn) {
        this._tabList.set(tabBtn, null);
    },
    setActive:function (tabBtn) {
        if(tabBtn === this._currentTab) return;

        if(this._tabList.exist(tabBtn)) {
            var allTabs = this._tabList.allKeys();
            for(var i = 0; i < allTabs.length; i++){
                var tab = allTabs[i];
                tab.setBright(false);
            }

            this._currentTab = tabBtn;
            this._currentTab.setBright(true);
            this._tabList.get(tabBtn)();
        }
    }
});
