/**
 * Created by nguyenquangninh on 8/12/16.
 */
var UIView = ccui.Widget.extend({
    onExit:function() {
        this._super();
    },
    setupView: function() {
        //Override by subclasses
    },
    parseCSB: function(node) {
        var children = node.getChildren().slice();
        node.removeAllChildren();

        for(var i = 0; i < children.length; i++)
        {
            var child = children[i];
            this.addChild(child);
        }

        // copy properties
        var com = node.getComponent("__ui_layout");
        if(!cc.isUndefined(com))
        {
            com.setOwner(null);
            this.addComponent(com);
        }

        this.setPosition(node.getPosition());
        this.setScale(node.getScaleX(), node.getScaleY());
        this.setRotation(node.getRotation());
        this.setName(node.getName());
        this.setVisible(node.isVisible());

        // end copy
        var parent = node.getParent();
        if (parent !== null && parent !== undefined) {
            children = parent.getChildren().slice();
            parent.removeAllChildren();
            for(i = 0; i < children.length; i++)
            {
                child = children[i];
                if (child === node) {
                    parent.addChild(this);
                }
                else {
                    parent.addChild(child);
                }
            }
        }

        this.setupView();
        this._refreshLayout();
    },
    _refreshLayout: function() {
        var lastContentSize = this.getContentSize();
        var contentSize = cc.size(0,0);
        var children = this.getChildren();
        for(var i = 0; i < children.length; i++)
        {
            var child = children[i];
            contentSize.width = Math.max(contentSize.width, child.getContentSize().width);
            contentSize.height = Math.max(contentSize.height, child.getContentSize().height);
        }

        this.setContentSize(contentSize);

        //reorganize child
        var dx = (contentSize.width - lastContentSize.width)* this.getAnchorPoint().x ;
        var dy = (contentSize.height - lastContentSize.height)* this.getAnchorPoint().y;
        for(i = 0; i < children.length; i++) {
            child = children[i];
            child.x += dx;
            child.y += dy;
        }

        // //Debug
        //var contentSize = this.getContentSize();
        // var yellowBox = new cc.LayerColor(cc.color(255,255,0,50), contentSize.width, contentSize.height);
        // yellowBox.setAnchorPoint(0,0);
        // yellowBox.setPosition(0,0);
        // this.addChild(yellowBox);
    },
    clone: function() {
        var cloneObj = ccui.Widget.prototype.clone.call(this);
        cloneObj.setupView = this.setupView;
        cloneObj.alignCenterInParent = this.alignCenterInParent;
        cloneObj.alignCenterInScreen = this.alignCenterInScreen;
        cloneObj.setupView();
        return cloneObj;
    },
    alignCenterInParent: function() {
        var v = {x:0.5, y:0.5};
        this.setAnchorPoint(v);
        this.setPositionType(cc.Widget.POSITION_PERCENT);
        this.setPositionPercent(v);
    },
    alignCenterInScreen:function() {
        // var v = {x:0.5, y:0.5};
        // this.setAnchorPoint(v);
        // var size = cc.director.getVisibleSize();
        // v.x = size.width / 2;
        // v.y = size.height / 2;
        // this.setPosition(v);
        var size = cc.director.getVisibleSize();
        this.setPosition(size.width/2, size.height/2);
    },
    addEventListener:function(eventName, callback) {
        cc.eventManager.addListener({
            event: cc.EventListener.CUSTOM,
            eventName: eventName,
            callback: callback
        }, this);
    },
    dispatchEvent: function(eventName, data) {
        cc.eventManager.dispatchCustomEvent(eventName, data);
    }
});

UIView.load = function (className, fileName) {
    return UIView.create(className, ccs.load(fileName).node);
}

UIView.loadAsync = function (className, fileName, completeCallback) {
    cc.loader.load(fileName, function () {
        var view = UIView.create(className, ccs.load(fileName).node);
        completeCallback(view);
    });
}

UIView.create = function (className, node) {

    if(node === null || node === undefined) {
        return null;
    }

    var view = null;

    if(node instanceof className) {
        view = node;
    }
    else {
        view = new className();
        view.parseCSB(node);
    }

    return view;
}
