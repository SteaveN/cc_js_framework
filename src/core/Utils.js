/**
 * Created by nguyenquangninh on 8/13/16.
 */
var Utils = Utils || {
    printStackTrace: function(msg) {
        var e = new Error(msg);
        cc.log(e.stack);
    }
}