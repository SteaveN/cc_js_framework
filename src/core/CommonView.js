/**
 * Created by nguyenquangninh on 8/13/16.
 */
var BMFButton = UIView.extend({
    _btn: null,
    _txtTitle: null,
    _autoResize: true,
    setupView: function () {
        this._btn = this.getChildByName("btn");
        this._txtTitle = this.getChildByName("txtTitle");
        this._autoResize = true;
    },
    clone: function () {
        var cloneObj = this._super();
        cloneObj.addClickEventListener = this.addClickEventListener;
        cloneObj.setTitle = this.setTitle;
        cloneObj.setAutoResize = this.setAutoResize;
        cloneObj.setWidth = this.setWidth;
        cloneObj.setBright = this.setBright;
        return cloneObj;
    },
    setTitle: function (stringID) {
        this._txtTitle.setString(stringID);
        if (this._autoResize) {
            this.setWidth(this._txtTitle.getContentSize().width + 100);
        }
    },
    addClickEventListener: function (callback) {
        this._btn.addClickEventListener(callback);
    },
    setWidth: function (value) {
        var size = this._btn.getContentSize();
        size.width = Math.max(220, value);
        this._btn.setContentSize(size);
        this._refreshLayout();
    },
    setAutoResize: function (value) {
        this._autoResize = value;
    },
    setBright: function (value) {
        if (this._btn !== null) {
            this._btn.setBright(value);
        }
    }
});

var ContentSwitcher = UIView.extend({
    _currentIndex: -1,
    setupView: function () {
        this._currentIndex = -1;
        this.to(0);
    },
    clone: function () {
        var cloneObj = this._super();
        cloneObj.to = this.to;
        cloneObj.getCurrentNode = this.getCurrentNode;
        return cloneObj;
    },
    to: function (index) {
        if (this._currentIndex != index) {
            this._currentIndex = index;
            var children = this.getChildren();

            for (var i = 0; i < children.length; ++i) {
                children[i].setVisible(i == index);
            }
        }
    },
    getCurrentNode: function () {
        if (this._currentIndex > -1 && this._currentIndex < this.getChildrenCount()) {
            return this.getChildren()[this._currentIndex];
        }
        return null;
    }
});

var UIBlockLayer = UIView.extend({
    _blackLayer:null,
    ctor: function () {
        this._super();
        this._blackLayer = new ccui.Layout();
        this._blackLayer.retain();
        this._blackLayer.setSwallowTouches(true);
        this._blackLayer.setTouchEnabled(true);
        this._blackLayer.setBackGroundColor(cc.color(0,0,0));
        this._blackLayer.setBackGroundColorType(ccui.Layout.BG_COLOR_SOLID);
        this._blackLayer.setOpacity(128);
        this._blackLayer.setContentSize(cc.winSize);
        this._blackLayer.addClickEventListener(function (ref) {
            // Close if click outside
        });
    },
    onExit: function () {
        this._super();
        this._blackLayer.removeFromParent();
        this._blackLayer.release();
        this._blackLayer = null;
    }
});

var UIPopup = UIBlockLayer.extend({
    _bg: null,
    _closeBtn: null,
    _closeBehavior: null,
    setupView: function () {
        this._txtTitle = this.getChildByName("txtTitle");
        this._bg = this.getChildByName("background");
        this._closeBehavior = UIPopup.CLOSE_BEHAVIOR.REMOVE;
        this._closeBtn = this.getChildByName("closeBtn");
        this._closeBtn.addClickEventListener(function (ref) {
            switch (this._closeBehavior) {
                case UIPopup.CLOSE_BEHAVIOR.REMOVE:
                    this.hide();
                    break;
                case UIPopup.HIDE:
                    this.setVisible(false);
                    break;
            }
        }.bind(this));
    },
    clone: function () {
        var cloneObj = this._super();
        cloneObj.show = this.show;
        cloneObj.hide = this.hide;
        cloneObj.setTitle = this.setTitle;
        return cloneObj;
    },
    show: function (onFinishCallback) {
        cc.director.getRunningScene().addChild(this._blackLayer);
        cc.director.getRunningScene().addChild(this);
        this.alignCenterInScreen();
        this.setScale(0);
        this.runAction(cc.sequence(cc.scaleTo(0.15, 1.1), cc.scaleTo(0.15, 1.0), cc.callFunc(function (node) {
            if (!cc.isUndefined(onFinishCallback)) {
                onFinishCallback();
            }
        }, this)));
    },
    hide: function (onFinishCallback) {
        this._blackLayer.removeFromParent();
        this.runAction(cc.fadeOut(0.15 * 2));
        this.runAction(cc.sequence(cc.scaleTo(0.15, 1.1), cc.scaleTo(0.15, 0), cc.delayTime(0.15), cc.callFunc(function (node) {
            node.removeFromParent();
            if (!cc.isUndefined(onFinishCallback)) {
                onFinishCallback();
            }
        }, this)));
    },
    setTitle: function (titleString) {
        this._txtTitle.setString(titleString);
    }
});

UIPopup.CLOSE_BEHAVIOR = {
    HIDE: 0,
    REMOVE: 1
};

var UIDialog = UIBlockLayer.extend({
    _bg: null,
    _txtTitle: null,
    _txtMessage: null,
    _userData:null,
    setupView: function () {
        this._bg = this.getChildByName("background");
        this._txtTitle = this.getChildByName("txtTitle");
        this._txtMessage = this.getChildByName("txtMessage");
    },
    clone: function () {
        var cloneObj = this._super();
        cloneObj.show = this.show;
        cloneObj.hide = this.hide;
        return cloneObj;
    },
    show:function (options) {

        var uConfig = options || UIDialog._defaultCongfig;
        var dConfig = UIDialog._defaultCongfig;

        this._userData = uConfig.userData;

        if (uConfig.message) {
            var uMessageLabel = uConfig.message;
            var messageLabel = dConfig.message;
            var finalMessage = typeof uMessageLabel.text != "undefined" ? uMessageLabel.text : messageLabel.text;
            this._txtMessage.setString(finalMessage);
        }
        if (uConfig.title) {
            var uTitleLabel = uConfig.title;
            var titleLabel = dConfig.title;
            var finalTitle = typeof uTitleLabel.text != "undefined" ? uTitleLabel.text : titleLabel.text;
            this._txtTitle.setString(finalTitle);
        }

        cc.director.getRunningScene().addChild(this._blackLayer);
        cc.director.getRunningScene().addChild(this);
        this.alignCenterInScreen();
        this.setScale(0);
        this.runAction(cc.sequence(cc.scaleTo(0.15, 1.1), cc.scaleTo(0.15, 1.0), cc.callFunc(function (node) {

        }, this)));
    },
    hide: function () {
        this._blackLayer.removeFromParent();
        this.runAction(cc.fadeOut(0.15 * 2));
        this.runAction(cc.sequence(cc.scaleTo(0.15, 1.1), cc.scaleTo(0.15, 0), cc.delayTime(0.15), cc.callFunc(function (node) {
            node.removeFromParent();

        }, this)));
    }
});

UIDialog._defaultCongfig = {
    userData: null,
    confirmBtn: {
        text: "Dong Y",
        textColor: null,
        callback: function (data) {}
    },
    cancelBtn: {
        text: "Huy",
        textColor: null,
        callback: function (data) {}
    },
    message: {
        text: "",
        color: null
    },
    title: {
        text: "",
        color: null
    }
};

UIDialog.confirm = function (message, title, confirmCb, cancelCb, userData) {
    var dialog = UIView.load(UIConfirmDialog, res.UIDialog);

    var options = {
        userData: userData,
        confirmBtn: {
            text: "Dong Y",
            callback: confirmCb
        },
        cancelBtn: {
            text: "Huy",
            callback: cancelCb
        },
        message: {
            text: message
        },
        title: {
            text: title
        }
    }

    dialog.show(options);
};

UIDialog.notify = function (message, title, confirmCb, userData) {
    var dialog = UIView.load(UIOkDialog, res.UIDialog);

    var options = {
        userData: userData,
        confirmBtn: {
            text: "Dong Y",
            callback: confirmCb
        },
        message: {
            text: message
        },
        title: {
            text: title
        }
    }

    dialog.show(options);
};

var UIOkDialog = UIDialog.extend({
    _okBtn: null,
    _okCallback:null,
    setupView: function () {
        this._super();
        this._okBtn = UIView.create(BMFButton,this.getChildByName("okBtn"));
        this._okBtn.setAutoResize(true);
        this._okBtn.addClickEventListener(function (ref) {
            this.hide();
            if(!cc.isUndefined(this._okCallback)) {
                this._okCallback(this._userData);
            }
        }.bind(this));
    },
    show:function (options) {
        this._super(options);
        var uConfig = options || UIDialog._defaultCongfig;
        var dConfig = UIDialog._defaultCongfig;

        if (uConfig.confirmBtn) {
            var uConfirmBtn = uConfig.confirmBtn;
            var dConfirmBtn = dConfig.confirmBtn;
            var fTitle = typeof uConfirmBtn.text != "undefined" ? uConfirmBtn.text : dConfirmBtn.text;
            this._okBtn.setTitle(fTitle);
            this._okCallback = uConfirmBtn.callback ? uConfirmBtn.callback : dConfirmBtn.callback;
        }
    }
});

var UIConfirmDialog = UIDialog.extend({
    _acceptBtn: null,
    _acceptCallback:null,
    _cancelBtn: null,
    _cancelCallback:null,
    setupView: function () {
        this._super();
        this._acceptBtn = UIView.create(BMFButton, this.getChildByName("acceptBtn"));
        this._acceptBtn.setAutoResize(true);
        this._acceptBtn.addClickEventListener(function (ref) {
            this.hide();
            if(!cc.isUndefined(this._acceptCallback)) {
                this._acceptCallback(this._userData);
            }
        }.bind(this));

        this._cancelBtn = UIView.create(BMFButton, this.getChildByName("cancelBtn"));
        this._cancelBtn.setAutoResize(true);
        this._cancelBtn.addClickEventListener(function (ref) {
            this.hide();
            if(!cc.isUndefined(this._cancelCallback)) {
                this._cancelCallback(this._userData);
            }
        }.bind(this));
    },
    show:function (options) {
        this._super(options);
        var uConfig = options || UIDialog._defaultCongfig;
        var dConfig = UIDialog._defaultCongfig;

        if (uConfig.confirmBtn) {
            var uConfirmBtn = uConfig.confirmBtn;
            var dConfirmBtn = dConfig.confirmBtn;
            var fTitle = typeof uConfirmBtn.text != "undefined" ? uConfirmBtn.text : dConfirmBtn.text;
            this._acceptBtn.setTitle(fTitle);
            this._acceptCallback = uConfirmBtn.callback ? uConfirmBtn.callback : dConfirmBtn.callback;
        }
        if (uConfig.cancelBtn) {
            var uCancelBtn = uConfig.cancelBtn;
            var dCancelBtn = dConfig.cancelBtn;
            var fCancelTitle = typeof uCancelBtn.text != "undefined" ? uCancelBtn.text : dCancelBtn.text;
            this._cancelBtn.setTitle(fCancelTitle);
            this._cancelCallback = uCancelBtn.callback ? uCancelBtn.callback : dCancelBtn.callback;
        }
    }
});

var Toasts = UIView.extend({
    _txtMessage:null,
    _background: null,
    _timeOutID1:-1,
    _timeOutID2:-1,
    setupView: function () {
        this._background = this.getChildByName("background");
        this._txtMessage = this.getChildByName("txtMessage");
        if(cc.sys.isNative)
        {
            this._txtMessage.getVirtualRenderer().setMaxLineWidth(cc.winSize.width * 0.8);
            this._txtMessage.getVirtualRenderer().setVerticalAlignment(cc.VERTICAL_TEXT_ALIGNMENT_CENTER);
        }
        else {
            this._txtMessage.getVirtualRenderer().setBoundingWidth(cc.winSize.width * 0.8);
        }
        this._txtMessage.getVirtualRenderer().setAlignment(cc.TEXT_ALIGNMENT_CENTER);
        this._background.setSwallowTouches(false);
    },
    clone: function () {
        var cloneObj = this._super();
        cloneObj.show = this.show;
        return cloneObj;
    },
    show: function (text, durationSec) {
        var duration = durationSec || 1;

        this._txtMessage.setString(text);
        var stringSize = this._txtMessage.getContentSize();
        stringSize.width = Math.max(220, stringSize.width * 1.2);
        stringSize.height = Math.max(53, stringSize.height);
        this._background.setContentSize(stringSize);
        //this.retain();
        clearInterval(this._timeOutID1);
        clearInterval(this._timeOutID2);

        //Fix crash: modify child list of current running scene while transition
        this._timeOutID1 = setTimeout(function () {
            this.removeFromParent(true);
            cc.director.getRunningScene().addChild(this);
            this.alignCenterInScreen();
            this.setScale(0, 1);
            this.runAction(cc.sequence(cc.scaleTo(0.15, 1.1, 1), cc.scaleTo(0.15, 1.0, 1), cc.delayTime(duration), cc.callFunc(function(node){
                node.hide();
            },this)));
            //this.release();
        }.bind(this), 2);

        this._timeOutID2 = setTimeout(function () {
            this.removeFromParent(true);
        }.bind(this), 1000* (duration + 0.8));
    },
    hide: function () {
        this.runAction(cc.fadeOut(0.15 * 2));
        this.runAction(cc.sequence(cc.scaleTo(0.05, 1.1, 1), cc.scaleTo(0.05, 0, 1), cc.delayTime(0.15), cc.callFunc(function (node) {
            node.removeFromParent();
        }, this)));
    }

});

Toasts.show = function (text) {
    Toasts.getInstance().show(text);
};

Toasts._instance = null;
Toasts.getInstance = function () {
    if(Toasts._instance === null)
    {
        Toasts._instance = UIView.load(Toasts, res.Toasts);
        Toasts._instance.retain();
    }

    return Toasts._instance;
};

Toasts.destroyInstance = function () {
    Toasts._instance.removeFromParent(true);
    Toasts._instance.release();
};

var ComboBox = UIView.extend({
    _btn:null,
    _listView: null,
    _title: null,
    _dataList:{},
    setupView: function () {
        this._btn = this.getChildByName("btn");
        this._btn.addClickEventListener(function (ref) {
            this._listView.setVisible(!this._listView.isVisible());
        }.bind(this));
        this._listView = this.getChildByName("list");
        this._listView.setVisible(false);
        this._title = this._btn.getChildByName("title");
    },
    clone: function () {
        var cloneObj = this._super();
        cloneObj.setTitle = this.setTitle;
        cloneObj.addData = this.addData;
        return cloneObj;
    },
    setTitle: function (text) {
        this._title.setString(text);
    },
    addData: function (text, data) {
        var item = new ccui.TextBMFont(text, "res/ui/Roboto_bold.fnt");
        item.setAnchorPoint(0,0);
        item.setTouchEnabled(true);
        item.setSwallowTouches(false);
        item.addClickEventListener(function (ref) {
            this._title.setString(item.getString());
            this._listView.setVisible(false);
        }.bind(this));

        this._dataList[text] = data;

        this._listView.pushBackCustomItem(item);
        this._listView.doLayout();
    },
    getData: function () {
        return this._dataList[this._title.getString()];
    }
});

var ComboBoxItem = UIView.extend({
    _text:null,
    _data:null,
    setupView: function () {
        this._text = this.getChildByName("text");
        this.setContentSize(this._text.getContentSize());
    },
    clone: function () {
        var cloneObj = this._super();
        return cloneObj;
    },
    setData: function(text,data) {
        this._text.setString(text);
        this._data = data;
    },
    getData:function () {
        return this._data;
    },
    addClickEventListener: function (callback) {
        this._text.addClickEventListener(callback);
    },
    getText:function () {
        return this._text.getString();
    }
});
