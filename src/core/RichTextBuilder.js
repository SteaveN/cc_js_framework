
var RichTextBuilder = cc.Class.extend({
    build:function (text, color, opacity) {
        var richText = new ccui.RichText();
        //richText.setTextHorizontalAlignment(cc.TEXT_ALIGNMENT_CENTER);
        //richText.setTextVerticalAlignment(cc.VERTICAL_TEXT_ALIGNMENT_CENTER);
        richText.ignoreContentAdaptWithSize (true);
        // richText.width = cc.winSize.width;
        // richText.height = 100;

        var bbCodes = text.match(RichTextBuilder.bbCodeRegex);
        var textOutside = text.split(RichTextBuilder.bbCodeRegex);

        var element = null;
        for(var i = 0; i < textOutside.length; i++)
        {
            var str = textOutside[i];
            element = this._buildNormalElement(str, color,opacity);
            richText.pushBackElement(element);

            if(i < bbCodes.length)
            {
                element = this._buildColorElement(bbCodes[i], opacity);
                richText.pushBackElement(element);
            }
        }

        return richText;
    },
    _buildNormalElement: function (text, color, opacity) {
        var textBMF = new ccui.TextBMFont(text, "res/ui/Roboto_bold.fnt");
        var element = new ccui.RichElementCustomNode(0, color, opacity, textBMF);
        return element;
    },
    _buildColorElement: function (text, opacity) {
        var hexArr = text.match(RichTextBuilder.colorRegex);
        if(hexArr.length > 0)
        {
            var textInside = text.match(RichTextBuilder.textInsideRegex)[0].replace(/[\]|\[]/g,'');
            var rgb = this._hexToRgb(hexArr[0]);
            var color = cc.color(rgb.r,rgb.g,rgb.b);

            var textBMF = new ccui.TextBMFont(textInside, "res/ui/Roboto_bold.fnt");
            var element = new ccui.RichElementCustomNode(0, color, 200, textBMF);
            return element;
        }

        return null;
    },
    _hexToRgb: function(hex) {
        // Expand shorthand form (e.g. "03F") to full form (e.g. "0033FF")
        var shorthandRegex = /^#?([a-f\d])([a-f\d])([a-f\d])$/i;
        hex = hex.replace(shorthandRegex, function(m, r, g, b) {
            return r + r + g + g + b + b;
        });

        var result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex);
        return result ? {
            r: parseInt(result[1], 16),
            g: parseInt(result[2], 16),
            b: parseInt(result[3], 16)
        } : null;
    }
});

RichTextBuilder.bbCodeRegex = /[color=#[abcdefABCDEF0-9]*\][a-zA-Z0-9 ]+\[\/color\]/g;
RichTextBuilder.colorRegex = /#[abcdefABCDEF0-9]+/g;
RichTextBuilder.textInsideRegex = /(\][a-zA-Z0-9 ]+\[)/g;
