/**
 * Created by nguyenquangninh on 8/24/16.
 */

var RadioGroup = cc.Class.extend({
    _group: null,
    _current: null,
    ctor:function () {
        this._group = new HashTable();
    },
    add:function (radioBtn, data) {
        this._group.set(radioBtn, data);
        radioBtn.addClickEventListener(function (ref) {
            this.setActive(radioBtn);
        }.bind(this));
    },
    setActive:function (radioBtn) {
        if(radioBtn === this._current) return;

        if(this._group.exist(radioBtn)) {
            var allBtn = this._group.allKeys();
            for(var i = 0; i < allBtn.length; i++){
                var btn = allBtn[i];
                btn.setBright(false);
            }

            this._current = radioBtn;
            this._current.setBright(true);
        }
    },
    getValue: function () {
        if(this._current !== null && this._group.exist(this._current)) {
            return this._group.get(this._current);
        }

        return null;
    }
});