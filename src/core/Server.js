/**
 * Created by nguyenquangninh on 8/30/16.
 */

var Server = cc.Class.extend({
    _address: "",
    ctor: function (address) {
        this._address = address;
    },
    connect: function () {

    },
    disconnect: function () {

    },
    send: function (requestType, data) {

    }
});

var HttpServer = Server.extend({
    ctor: function (address) {
        this._super(address);
    },
    send: function (requestType, data, method) {
        this._super(requestType, data);
        data = data || {};
        var requestMethod = method || HttpServer.REQUEST_METHOD.GET;
        var req = new HttpRequest(requestType, this._address + requestType, data, requestMethod);
        req.setCallback(this.onReceiveResponse, this.onRequestTimeout);
        req.doRightNow(true);
    },
    onReceiveResponse: function (request, responseData) {

        if (request === null || responseData === null) {
            // UIDialog.notify("Báo lỗi","Không thể kết nối đến server", function (data) {
            // });
            return;
        }
        var tag = request.getTag();
        responseData = responseData.replace("\n", "\\n");

        var json = null;
        try {
            json = JSON.parse(responseData);
        } catch (err) {
            cc.log("Server JSON.parse(response) error: " + err.message);
        }

            cc.log("Server response text: " + responseData);
            cc.log("Server response tag: " + tag + " content: \n" + JSON.stringify(json, null, 2));

        var responsePacket = {
            type: tag,
            data: json
        };
        cc.eventManager.dispatchCustomEvent(HttpServer.SERVER_DATA_EVENT, responsePacket);
    },
    onRequestTimeout: function () {

    }
});

HttpServer.Instance = new HttpServer("https://sloto-st.cuamobi.com/service/");

HttpServer.SERVER_DATA_EVENT = "SERVER_DATA_EVENT";
HttpServer.REQUEST_METHOD = {
    GET: "GET",
    POST: "POST"
};
HttpServer.STATUS_CODE = {
    NA : -2,
    OK : 200,
    PAGE_NOT_FOUND: 404,
    TIME_OUT: 408,
    SERVER_DOWN: 502,
    LOST_CONNECTION: 0,
};

