/**
 * Created by nguyenquangninh on 8/22/16.
 * Hashtable which accept object as key.
 */
var HashTable = cc.Class.extend({
    _keys:[],
    _hash:{},
    set: function (key, value) {
        if(value === null) {
            if(this._keys.indexOf(key) !== -1) {
                this._keys.splice(this._keys.indexOf(key),1);
            }
        } else {
            if(!this.exist(key)) {
                this._keys.push(key);
            }
        }

        if(typeof key === "string"){
            this._hash[key] = value;
        }
        else{
            if(key._hashtableUniqueId == undefined){
                key._hashtableUniqueId = HashTable.getID();
            }
            this._hash[key._hashtableUniqueId] = value;
        }
    },
    get: function (key) {
        if(typeof key === "string"){
            return this._hash[key];
        }
        if(key._hashtableUniqueId == undefined){
            return undefined;
        }
        return this._hash[key._hashtableUniqueId];
    },
    allKeys: function () {
        return this._keys;
    },
    exist: function (key) {
        return this.get(key) !== undefined && this.get(key) !== null;
    }
});

HashTable._id = 0;
HashTable.getID = function(){
    return (++HashTable._id).toString();
};
