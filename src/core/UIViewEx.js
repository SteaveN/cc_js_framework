/**
 * Created by nguyenquangninh on 8/12/16.
 */
var UIView = ccui.Widget.extend({
    _innerNode:null,
    onExit:function() {
        this._super();
        this._innerNode = null;
    },
    setupView: function() {
        //Override by subclasses
    },
    parseCSB: function(node) {

        this._innerNode = node;

        // copy properties
        // var com = node.getComponent("__ui_layout");
        // com.setOwner(null);
        // this.addComponent(com);

        this.setPosition(node.getPosition());
        this.setScale(node.getScaleX(), node.getScaleY());
        this.setRotation(node.getRotation());
        this.setName(node.getName());
        this.setVisible(node.isVisible());

        node.setPosition(0,0);
        node.setScale(1,1);
        node.setRotation(0);
        node.setVisible(true);

        // end copy
        var parent = node.getParent();
        if (parent !== null && parent !== undefined) {
            var children = parent.getChildren().slice();
            parent.removeAllChildren();
            for(var i = 0; i < children.length; i++)
            {
                var child = children[i];
                if (child === node) {
                    this.addChild(node);
                    parent.addChild(this);
                }
                else {
                    parent.addChild(child);
                }
            }
        }
        else {
            this.addChild(node);
        }

        this.setupView();
        var contentSize = cc.size(0,0);
        children = node.getChildren();
        for(i = 0; i < children.length; i++)
        {
            child = children[i];
            contentSize.width = Math.max(contentSize.width, child.getContentSize().width);
            contentSize.height = Math.max(contentSize.height, child.getContentSize().height);
        }

        this.setContentSize(contentSize);

        //reorganize child
        var dx = contentSize.width * this.getAnchorPoint().x;
        var dy = contentSize.height * this.getAnchorPoint().y;
        node.x +=dx;
        node.y +=dy;

        //Debug
        var yellowBox = new cc.LayerColor(cc.color(255,255,0,50), contentSize.width, contentSize.height);
        yellowBox.setAnchorPoint(0,0);
        yellowBox.setPosition(0,0);
        this.addChild(yellowBox);

    },
    getChildByName: function(name){
        return this._innerNode.getChildByName(name);
    },
    clone: function() {
        var cloneObj = ccui.Widget.prototype.clone.call(this);
        cloneObj.setupView = this.setupView;
        cloneObj.alignCenterInParent = this.alignCenterInParent;
        cloneObj.alignCenterInScreen = this.alignCenterInScreen;
        cloneObj.setupView();
        return cloneObj;
    },
    alignCenterInParent: function() {
        var v = {x:0.5, y:0.5};
        this.setAnchorPoint(v);
        this.setPositionType(cc.Widget.POSITION_PERCENT);
        this.setPositionPercent(v);
    },
    alignCenterInScreen:function() {
        var v = {x:0.5, y:0.5};
        this.setAnchorPoint(v);
        var size = cc.director.getVisibleSize();
        v.x = size.width / 2;
        v.y = size.height / 2;
        this.setPosition(v);
    }
});

UIView.load = function (className, fileName) {
    return UIView.create(className, ccs.load(fileName).node);
}

UIView.create = function (className, node) {

    if(node === null || node === undefined) {
        return null;
    }

    var view = null;

    if(node instanceof className) {
        view = node;
    }
    else {
        view = new className();
        view.parseCSB(node);
    }

    return view;
}
