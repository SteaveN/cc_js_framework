/**
 * Created by admin on 1/25/16.
 */
//var HTTP_REQUEST_TYPE = {
//     NA :"N/A",
//     GET : "GET",
//     POST : "POST"
//    };
//
//
//var HTTP_REQUEST_READYSTATE = {
//    NA : -1,
//    UNSENT : 0,
//    OPENED : 1,
//    HEADERS_RECEIVED: 2,
//    LOADING: 3,
//    DONE: 4,
//};
//
//var HTTP_REQUEST_STATUS = {
//    NA : -2,
//    OK : 200,
//    PAGE_NOT_FOUND: 404,
//    TIME_OUT: 408,
//    SERVER_DOWN: 502,
//    LOST_CONNECTION: 0,
//};
//
//var TIMEOUT = 30000;
//var REQUEST_HEADER = "Content-Type";
//
//var REQUEST_HEADER_VALUE = {
//    JSON : "application/json; charset=utf-8",
//    FORM_DATA: "multipart/form-data; boundary=C4CA8000",
//};

var HTTP_REQUEST_TYPE = {
    NA :"N/A",
    GET : "GET",
    POST : "POST"
};


var HTTP_REQUEST_READYSTATE = {
    NA : -1,
    UNSENT : 0,
    OPENED : 1,
    HEADERS_RECEIVED: 2,
    LOADING: 3,
    DONE: 4,
};

var HTTP_REQUEST_STATUS = {
    NA : -2,
    OK : 200,
    PAGE_NOT_FOUND: 404,
    TIME_OUT: 408,
    SERVER_DOWN: 502,
    LOST_CONNECTION: 0,
};

var TIMEOUT = 30000;
var REQUEST_HEADER = "Content-Type";

var REQUEST_HEADER_VALUE = {
    JSON : "application/json; charset=utf-8",
    FORM_DATA: "multipart/form-data; boundary=C4CA8000",
};
var HttpRequest = cc.Class.extend({

    m_server: null,
    m_httpRequestType: HTTP_REQUEST_TYPE.NA, //enum
    m_address: "",
    m_params: null,
    m_tag: "",
    m_stringCallback: null,
    m_timeoutCallback: null,
    m_severDownCallBack: null,
    m_loadingPopup: true,
    m_hanldeTimeout: true,
    m_hideBlackColorOfLoading: false,

    m_startRequestTime: null,

    ctor: function (tag, address, params, requestType)
    {
        this.m_address = address;
        this.m_params = params;
        this.m_httpRequestType = requestType;
        this.m_tag = tag;

        // if (HTTP_LIST_REQUEST_NOT_SHOW_LOG.indexOf(this.m_tag) == -1) {
        //     cc.log("HttpRequest Created - - - " + this.m_address);
        // }
    },

    setCallback: function(callback, timeoutCallback, severDownCallBack)
    {
        this.m_stringCallback  = callback;
        this.m_timeoutCallback = timeoutCallback;
        if (cc.isUndefined(this.m_timeoutCallback)) {
            this.m_timeoutCallback = null;
        }

        this.m_severDownCallBack = severDownCallBack;
        if (cc.isUndefined(this.m_severDownCallBack)) {
            this.m_severDownCallBack = null;
        }
    },

    getTag: function()
    {
        return this.m_tag;
    },

    getAllHeaders : function(server)
    {
        //list of header
        // var timeStartToRecieveRequest = server.getResponseHeader (KEY.PING);
        // //cc.log("timeStartToRevieveReqeust: " + timeStartToRecieveRequest);
        // //cc.log("time start to send request: " + this.m_startRequestTime);
        // //cc.log("ping: " + (timeStartToRecieveRequest - this.m_startRequestTime));
        // //cc.log("m_pingDelta: " + GameModel.getInstance().getGameInfo().m_pingDelta);
        //
        // if (GameModel.getInstance().getGameInfo().m_pingDelta == null) {
        //     GameModel.getInstance().getGameInfo().m_pingDelta = timeStartToRecieveRequest - this.m_startRequestTime;
        //     cc.log("---------- delta: " + GameModel.getInstance().getGameInfo().m_pingDelta);
        // }else {
        //     GameModel.getInstance().getGameInfo().m_ping = timeStartToRecieveRequest - this.m_startRequestTime;
        //     Utils.dispatchEventPing();
        // }
        //
        // //list of header log
        // if (HTTP_LIST_REQUEST_NOT_SHOW_LOG.indexOf(this.m_tag) == -1) {
        //     cc.log("HttpRequest ping time " + GameModel.getInstance().getGameInfo().m_ping);
        // }
    },

    onRequestTimeOut : function (thisclass, server)
    {
        server.ontimeout = function ()
        {
            // XMLHttpRequest timed out. Do something here.
            // if (HTTP_LIST_REQUEST_NOT_SHOW_LOG.indexOf(this.m_tag) == -1) {
            //     cc.error("The request for " + thisclass.m_address + " timed out.");
            // }
            //if (this.m_loadingPopup) {
            //    GLayerManager.getInstance().hideModuleID(ModuleIDDefine.LoadingModule);
            //}

            var resendAction = function (message) {
                var resend = function () {
                    // if (this.m_loadingPopup){
                    //     var running = cc.director.getRunningScene();
                    //     GLayerManager.getInstance().showModule(running, ModuleIDDefine.LoadingModule);
                    //     if (this.m_hideBlackColorOfLoading) {
                    //         LoadingLayer.getInstance().hideBackgroundColor();
                    //     }
                    // }

                    if (!this) {
                        cc.log("!thisclass");
                    }
                    if (!this.m_server) {
                        cc.log("!thisclass.m_server");
                    }
                    var req = cc.loader.getXMLHttpRequest();
                    req.timeout = 30000;

                    this.m_server = req;
                    this.onRequestCompleted(this, req);

                    this.m_server.open(HttpServer.REQUEST_METHOD.GET, this.m_address, true);
                    this.m_server.setRequestHeader(REQUEST_HEADER, REQUEST_HEADER_VALUE.JSON);
                    this.m_server.send();
                }.bind(thisclass);

                var deniedResend = function () {
                    if (this.m_timeoutCallback) {
                        this.m_timeoutCallback();
                    }
                }.bind(thisclass);

                if (!thisclass.m_hanldeTimeout) {
                    resend();
                    return;
                }
            };
            resendAction();
        }.bind(this);
    },

    onRequestCompleted : function (thisclass, server)
    {
        server.onreadystatechange = function()
        {
            if (server.responseText == null)
            {
                cc.log("HttpRequest Response null " + thisclass.m_address);
            }

            if (server.readyState == HTTP_REQUEST_READYSTATE.DONE)
            {
                switch(server.status)
                {
                    case HTTP_REQUEST_STATUS.OK:
                    {
                        thisclass.getAllHeaders(server);

                        if (thisclass.m_stringCallback)
                        {
                            thisclass.m_stringCallback(thisclass, server.responseText);
                        }
                        break;
                    }
                    case HTTP_REQUEST_STATUS.PAGE_NOT_FOUND:
                    {
                        break;
                    }
                    case HTTP_REQUEST_STATUS.TIME_OUT:
                    {
                        this.onRequestTimeOut(thisclass, server);
                        break;
                    }

                    case HTTP_REQUEST_STATUS.SERVER_DOWN:
                    {
                        if (this.m_severDownCallBack) {
                            this.m_severDownCallBack();
                            break;
                        }
                        break;
                    }

                    case HTTP_REQUEST_STATUS.LOST_CONNECTION:
                        thisclass.onRequestTimeOut(thisclass, server);
                        break;
                }
                if (thisclass.m_loadingPopup) {
                }
            }
            else if (server.readyState == HTTP_REQUEST_READYSTATE.LOADING) {
                //if (HTTP_LIST_REQUEST_NOT_SHOW_LOG.indexOf(thisclass.m_tag) == -1) {
                //    cc.warn("HTTP HTTP_REQUEST_READYSTATE.LOADING " + thisclass.m_address);
                //}
            }
            else if (server.readyState == HTTP_REQUEST_READYSTATE.UNSENT) {
                //if (HTTP_LIST_REQUEST_NOT_SHOW_LOG.indexOf(thisclass.m_tag) == -1) {
                //    cc.warn("HTTP HTTP_REQUEST_READYSTATE.UNSENT " + thisclass.m_address);
                //}
            }
            else if (server.readyState == HTTP_REQUEST_READYSTATE.HEADERS_RECEIVED) {
                //if (HTTP_LIST_REQUEST_NOT_SHOW_LOG.indexOf(thisclass.m_tag) == -1) {
                //    cc.warn("HTTP HTTP_REQUEST_READYSTATE.HEADERS_RECEIVED " + thisclass.m_address);
                //}
            }
            else if (server.readyState == HTTP_REQUEST_READYSTATE.NA) {
                //if (HTTP_LIST_REQUEST_NOT_SHOW_LOG.indexOf(thisclass.m_tag) == -1) {
                //    cc.warn("HTTP HTTP_REQUEST_READYSTATE.NA " + thisclass.m_address);
                //}
            }
            else if (server.readyState == HTTP_REQUEST_READYSTATE.OPENED) {
                //if (HTTP_LIST_REQUEST_NOT_SHOW_LOG.indexOf(thisclass.m_tag) == -1) {
                //    cc.warn("HTTP HTTP_REQUEST_READYSTATE.OPENED " + thisclass.m_address);
                //}
            }
            else {
                //if (HTTP_LIST_REQUEST_NOT_SHOW_LOG.indexOf(thisclass.m_tag) == -1) {
                //    cc.warn("HTTP HTTP_REQUEST_READYSTATE UNKNOW " + thisclass.m_address);
                //}
            }

        }
    },

    doRightNow: function(loading, needHandleTimeOut, hideBlackColorOfLoading)
    {
        // TODO: ========== Calculator delta time ========
        var date = new Date();

        // ===============================================
        this.m_hanldeTimeout = needHandleTimeOut;
        if (cc.isUndefined(this.m_hanldeTimeout)) {
            this.m_hanldeTimeout = true;
        }
        this.m_loadingPopup = loading;
        this.m_hideBlackColorOfLoading = hideBlackColorOfLoading;
        if (this.m_loadingPopup){
            var running = cc.director.getRunningScene();
            if (this.m_hideBlackColorOfLoading) {
            }
        }

        var req = cc.loader.getXMLHttpRequest();
        req.timeout = TIMEOUT;
        this.m_server = req;

        if(this.m_httpRequestType == HTTP_REQUEST_TYPE.GET)
        {

            var data = "";
            data += "?";

            for(var key in this.m_params)
            {
                var value = this.m_params[key];
                data += key + "=" + value + "&";
            }
            if(data.length)
            {
                this.m_address += data;
            }

            this.m_address = encodeURI(this.m_address);
            this.onRequestCompleted(this, req);

            req.open(HTTP_REQUEST_TYPE.GET, this.m_address, true);
            req.setRequestHeader(REQUEST_HEADER, REQUEST_HEADER_VALUE.JSON);
            req.send();

            req.onerror = function (req, textStatus, errorThrown) {
                cc.log("HttpRequest onerror");
            };
        }
        else
        {
            this.m_address = encodeURI(this.m_address);
            this.onRequestCompleted(this, req);

            this.onRequestTimeOut(this, req);

            req.onload = function () {
                if(req.readyState === 4)
                    req.status === 200 ? cc.log("req.onload ok") : cc.log("req.onload false");
            };

            req.onerror = function(){
                cc.log("req.onerror " + req.status);
            };

            req.open(HTTP_REQUEST_TYPE.POST, this.m_address, true);
            req.setRequestHeader(REQUEST_HEADER, REQUEST_HEADER_VALUE.FORM_DATA);

            var data = this.m_params["body"];

            req.send(data);

            cc.log("HttpRequest Send Post ----- "  + this.m_address);
        }
    },
});



