/**
 * Created by nguyenquangninh on 8/22/16.
 */
var TestRichText = cc.Scene.extend({
    onEnter: function () {
        this._super();
        // //Debug
        var yellowBox = new cc.LayerColor(cc.color(255,255,0,50),200,200);
        yellowBox.setAnchorPoint(0,0);
        yellowBox.setPosition(300,300);
        this.addChild(yellowBox);

        var builder = new RichTextBuilder();
        var richText = builder.build('Toi ten la [color=#ff00ff] Nguyen Steave[/color] Quang Ninh. Nam nay toi [color=#ff11aa]31[/color] tuoi', cc.color.WHITE, 255);

        this.addChild(richText);
        richText.setPosition(cc.winSize.width/2,cc.winSize.height/2);
    },
});
