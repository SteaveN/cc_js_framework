/**
 * Created by nguyenquangninh on 8/30/16.
 */
var GET_MAINTAIN_INFO = "system/getthongtinbaotri";

var TestServer = cc.Scene.extend({
    onEnter: function () {
        this._super();
        this.addEventListener(HttpServer.SERVER_DATA_EVENT, this.onServerData);

        var toast = UIView.load(BMFButton, res.BMFButton);
        toast.setTitle("Toast");
        toast.addClickEventListener(function (ref) {
            HttpServer.Instance.send(GET_MAINTAIN_INFO);
        });
        toast.setPosition(50,100);
        this.addChild(toast);
    },
    addEventListener:function(eventName, callback) {
        cc.eventManager.addListener({
            event: cc.EventListener.CUSTOM,
            eventName: eventName,
            callback: callback
        }, this);
    },
    onServerData:function (e) {
        var response = e.getUserData();
        switch (response.type) {
            case GET_MAINTAIN_INFO:
                Toasts.show(response.data.msg);
                break;
        }
    }
});
