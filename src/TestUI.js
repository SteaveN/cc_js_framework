/**
 * Created by nguyenquangninh on 8/13/16.
 */
var TestUIScene = cc.Scene.extend({
    m_btnCenter : null,
    m_Node:null,
    onEnter: function () {
        this._super();
        var sprite = new cc.Sprite(res.background);
        this.addChild(sprite);
        sprite.setPosition(cc.winSize.width/2, cc.winSize.height/2);
        var toast = UIView.load(BMFButton, res.BMFButton);
        toast.setTitle("Toast");
        toast.addClickEventListener(function (ref) {
            Toasts.show("Chuc mung ban da len cap 12");
        });
        toast.setPosition(50,100);
        this.addChild(toast);

        var popup = UIView.load(BMFButton, res.BMFButton);
        popup.setTitle("Popup");
        popup.addClickEventListener(function (ref) {
            var p = UIView.load(UIPopup,res.UIPopup);
            p.show();
        });
        popup.setPosition(300,100);
        this.addChild(popup);

        var confirm = UIView.load(BMFButton, res.BMFButton);
        confirm.setTitle("confirm");
        confirm.addClickEventListener(function (ref) {
            UIDialog.confirm("Ban co muon di tiep?","THONG BAO", function (data) {
                cc.log("Ok");
            }, function (data) {
                cc.log("cancel");
            }, 100);
        });
        confirm.setPosition(500,100);
        this.addChild(confirm);

        var notify = UIView.load(BMFButton, res.BMFButton);
        notify.setTitle("notify");
        notify.addClickEventListener(function (ref) {
            UIDialog.notify("Ban co muon di tiep?","THONG BAO", function (data) {
                cc.log("Ok");
            }, 100);
        });
        notify.setPosition(700,100);
        this.addChild(notify);
    },
});