var TestTabGroup = cc.Scene.extend({
    onEnter: function () {
        this._super();
        var layout = ccs.load(res.TestTab).node;
        this.addChild(layout);
        layout.setPosition(cc.winSize.width/2,cc.winSize.height/2);

        var questTab = UIView.create(BMFButton, layout.getChildByName("questTab"));
        questTab.setTitle("Quest");
        var mailTab = UIView.create(BMFButton, layout.getChildByName("mailTab"));
        mailTab.setTitle("Mail");
        var guildTab = UIView.create(BMFButton, layout.getChildByName("guildTab"));
        guildTab.setTitle("Guild");

        var questContainer = layout.getChildByName("questPanel");
        var mailContainer = layout.getChildByName("mailPanel");

        var tabGroup = new TabGroup();
        tabGroup.addTab(questTab, function () {
            questContainer.setVisible(true);
            mailContainer.setVisible(false);
        });
        tabGroup.addTab(mailTab, function () {
            questContainer.setVisible(false);
            mailContainer.setVisible(true);
        });

        tabGroup.addTab(guildTab, function () {
            questContainer.setVisible(false);
            mailContainer.setVisible(false);
            Toasts.show("Tinh nang dang bao tri");
        });
        tabGroup.setActive(mailTab);
    },
});
