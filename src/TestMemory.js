/**
 * Created by nguyenquangninh on 8/13/16.
 */
var TestMemory = cc.Scene.extend({
    _count:-1,
    _popup:null,
    onEnter: function () {
        this._super();
        this.scheduleUpdate();
    },
    update:function (dt) {
        this._count++;
        if(this._count % 2 === 0) {
            this._popup = UIView.load(UIPopup,res.UIPopup);
            this._popup.show();
        }
        else {
            this._popup.hide();
        }
    }
});