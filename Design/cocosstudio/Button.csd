<GameFile>
  <PropertyGroup Name="Button" Type="Node" ID="240e3129-1a64-49d7-9fdd-c3e6cd591cfe" Version="3.10.0.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000" />
      <ObjectData Name="Root" Tag="8" ctype="GameNodeObjectData">
        <Size X="0.0000" Y="0.0000" />
        <Children>
          <AbstractNodeData Name="Button" ActionTag="1107305217" Tag="91" IconVisible="False" LeftMargin="-90.0000" RightMargin="-90.0000" TopMargin="-31.0000" BottomMargin="-31.0000" TouchEnable="True" FontSize="22" ButtonText="Click me" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="150" Scale9Height="40" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
            <Size X="180.0000" Y="62.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <TextColor A="255" R="255" G="255" B="255" />
            <DisabledFileData Type="Normal" Path="BTN_Do.png" Plist="" />
            <PressedFileData Type="Normal" Path="BTN_Do.png" Plist="" />
            <NormalFileData Type="Normal" Path="BTN_Do.png" Plist="" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>