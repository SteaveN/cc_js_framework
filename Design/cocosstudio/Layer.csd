<GameFile>
  <PropertyGroup Name="Layer" Type="Layer" ID="63933c90-7166-43df-bad6-327514a3c97d" Version="3.10.0.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000" />
      <ObjectData Name="Layer" Tag="3" ctype="GameLayerObjectData">
        <Size X="960.0000" Y="640.0000" />
        <Children>
          <AbstractNodeData Name="bg" ActionTag="-247482776" Tag="5" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="82.9760" RightMargin="77.0240" TopMargin="68.9760" BottomMargin="71.0240" Scale9Enable="True" LeftEage="128" RightEage="132" TopEage="179" BottomEage="141" Scale9OriginX="128" Scale9OriginY="179" Scale9Width="127" Scale9Height="107" ctype="ImageViewObjectData">
            <Size X="800.0000" Y="500.0000" />
            <Children>
              <AbstractNodeData Name="okBtn" ActionTag="67638827" Tag="9" IconVisible="True" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="400.0000" RightMargin="400.0000" TopMargin="375.0000" BottomMargin="125.0000" StretchWidthEnable="False" StretchHeightEnable="False" InnerActionSpeed="1.0000" CustomSizeEnabled="False" ctype="ProjectNodeObjectData">
                <Size X="0.0000" Y="0.0000" />
                <AnchorPoint />
                <Position X="400.0000" Y="125.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5000" Y="0.2500" />
                <PreSize X="0.0000" Y="0.0000" />
                <FileData Type="Normal" Path="BMFButton.csd" Plist="" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="482.9760" Y="321.0240" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5031" Y="0.5016" />
            <PreSize X="0.8333" Y="0.7813" />
            <FileData Type="Normal" Path="2_Slt_Ketqua_panel.png" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="image" ActionTag="1631712723" Tag="9" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" TopMargin="-44.8000" BottomMargin="44.8000" ctype="SpriteObjectData">
            <Size X="960.0000" Y="640.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="480.0000" Y="364.8000" />
            <Scale ScaleX="0.4695" ScaleY="0.4461" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5000" Y="0.5700" />
            <PreSize X="1.0000" Y="1.0000" />
            <FileData Type="Normal" Path="HelloWorld.png" Plist="" />
            <BlendFunc Src="770" Dst="771" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>