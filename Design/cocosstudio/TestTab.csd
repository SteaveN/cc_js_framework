<GameFile>
  <PropertyGroup Name="TestTab" Type="Node" ID="958dd088-0894-4956-a8fd-83328b437c5c" Version="3.10.0.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000" />
      <ObjectData Name="Node" Tag="143" ctype="GameNodeObjectData">
        <Size X="0.0000" Y="0.0000" />
        <Children>
          <AbstractNodeData Name="background" ActionTag="-1416591961" Tag="160" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="-350.0000" RightMargin="-350.0000" TopMargin="-213.5000" BottomMargin="-213.5000" Scale9Enable="True" LeftEage="127" RightEage="127" TopEage="140" BottomEage="140" Scale9OriginX="127" Scale9OriginY="140" Scale9Width="133" Scale9Height="147" ctype="ImageViewObjectData">
            <Size X="700.0000" Y="427.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="2_Slt_Ketqua_panel.png" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="questPanel" ActionTag="-1567416953" Tag="173" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="-200.0000" RightMargin="-200.0000" TopMargin="-29.0004" BottomMargin="-170.9996" TouchEnable="True" ClipAble="False" BackColorAlpha="102" ComboBoxIndex="1" ColorAngle="90.0000" ctype="PanelObjectData">
            <Size X="400.0000" Y="200.0000" />
            <Children>
              <AbstractNodeData Name="snail1_1" ActionTag="-259780475" Tag="174" IconVisible="False" LeftMargin="150.9993" RightMargin="223.0007" TopMargin="105.5000" BottomMargin="69.5000" ctype="SpriteObjectData">
                <Size X="26.0000" Y="25.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="163.9993" Y="82.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.4100" Y="0.4100" />
                <PreSize X="0.0650" Y="0.1250" />
                <FileData Type="Normal" Path="snail1.png" Plist="" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position Y="-70.9996" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <SingleColor A="255" R="150" G="200" B="255" />
            <FirstColor A="255" R="150" G="200" B="255" />
            <EndColor A="255" R="255" G="255" B="255" />
            <ColorVector ScaleY="1.0000" />
          </AbstractNodeData>
          <AbstractNodeData Name="mailPanel" ActionTag="-1610153888" Tag="175" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="-200.0000" RightMargin="-200.0000" TopMargin="-28.0005" BottomMargin="-171.9995" TouchEnable="True" ClipAble="False" BackColorAlpha="102" ComboBoxIndex="1" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
            <Size X="400.0000" Y="200.0000" />
            <Children>
              <AbstractNodeData Name="snail2_3" ActionTag="13212960" Tag="177" IconVisible="False" LeftMargin="197.8328" RightMargin="179.1672" TopMargin="112.3555" BottomMargin="67.6445" ctype="SpriteObjectData">
                <Size X="23.0000" Y="20.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="209.3328" Y="77.6445" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5233" Y="0.3882" />
                <PreSize X="0.0575" Y="0.1000" />
                <FileData Type="Normal" Path="snail2.png" Plist="" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position Y="-71.9995" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <SingleColor A="255" R="150" G="200" B="255" />
            <FirstColor A="255" R="150" G="200" B="255" />
            <EndColor A="255" R="255" G="255" B="255" />
            <ColorVector ScaleY="1.0000" />
          </AbstractNodeData>
          <AbstractNodeData Name="questTab" ActionTag="-721828185" Tag="223" IconVisible="True" LeftMargin="-219.0000" RightMargin="219.0000" TopMargin="-130.5000" BottomMargin="130.5000" StretchWidthEnable="False" StretchHeightEnable="False" InnerActionSpeed="1.0000" CustomSizeEnabled="False" ctype="ProjectNodeObjectData">
            <Size X="0.0000" Y="0.0000" />
            <AnchorPoint />
            <Position X="-219.0000" Y="130.5000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="Tab.csd" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="mailTab" ActionTag="-1040615531" Tag="227" IconVisible="True" LeftMargin="-2.0000" RightMargin="2.0000" TopMargin="-130.5000" BottomMargin="130.5000" StretchWidthEnable="False" StretchHeightEnable="False" InnerActionSpeed="1.0000" CustomSizeEnabled="False" ctype="ProjectNodeObjectData">
            <Size X="0.0000" Y="0.0000" />
            <AnchorPoint />
            <Position X="-2.0000" Y="130.5000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="Tab.csd" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="guildTab" ActionTag="1222647556" Tag="231" IconVisible="True" LeftMargin="215.0000" RightMargin="-215.0000" TopMargin="-130.5000" BottomMargin="130.5000" StretchWidthEnable="False" StretchHeightEnable="False" InnerActionSpeed="1.0000" CustomSizeEnabled="False" ctype="ProjectNodeObjectData">
            <Size X="0.0000" Y="0.0000" />
            <AnchorPoint />
            <Position X="215.0000" Y="130.5000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="Tab.csd" Plist="" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>