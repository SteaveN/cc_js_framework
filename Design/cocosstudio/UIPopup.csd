<GameFile>
  <PropertyGroup Name="UIPopup" Type="Node" ID="c6881ea0-f1bf-4b76-8656-b1c326185afa" Version="3.10.0.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000" />
      <ObjectData Name="Node" Tag="57" ctype="GameNodeObjectData">
        <Size X="0.0000" Y="0.0000" />
        <Children>
          <AbstractNodeData Name="background" ActionTag="-1524644246" Tag="58" IconVisible="False" LeftMargin="-350.0000" RightMargin="-350.0000" TopMargin="-213.5000" BottomMargin="-213.5000" Scale9Enable="True" LeftEage="127" RightEage="127" TopEage="140" BottomEage="140" Scale9OriginX="127" Scale9OriginY="140" Scale9Width="133" Scale9Height="147" ctype="ImageViewObjectData">
            <Size X="700.0000" Y="427.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="2_Slt_Ketqua_panel.png" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="txtTitle" ActionTag="-766167550" Tag="59" IconVisible="False" LeftMargin="-163.5000" RightMargin="-163.5000" TopMargin="-176.5000" BottomMargin="123.5000" LabelText="Nhiem Vu Hang Ngay" ctype="TextBMFontObjectData">
            <Size X="327.0000" Y="53.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position Y="150.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <LabelBMFontFile_CNB Type="Normal" Path="Roboto_bold.fnt" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="HelloWorld_1" ActionTag="-1364957073" Tag="60" IconVisible="False" LeftMargin="-480.0000" RightMargin="-480.0000" TopMargin="-282.0000" BottomMargin="-358.0000" ctype="SpriteObjectData">
            <Size X="960.0000" Y="640.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position Y="-38.0000" />
            <Scale ScaleX="0.4000" ScaleY="0.4000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="HelloWorld.png" Plist="" />
            <BlendFunc Src="770" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="closeBtn" ActionTag="1915855484" Tag="61" IconVisible="False" LeftMargin="267.0000" RightMargin="-395.0000" TopMargin="-244.0000" BottomMargin="116.0000" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="482" Scale9Height="490" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
            <Size X="128.0000" Y="128.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="331.0000" Y="180.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <TextColor A="255" R="65" G="65" B="70" />
            <DisabledFileData Type="Normal" Path="close_button_1-512.png" Plist="" />
            <PressedFileData Type="Normal" Path="close_button_1-512.png" Plist="" />
            <NormalFileData Type="Normal" Path="close_button_1-512.png" Plist="" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>