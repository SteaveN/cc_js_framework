<GameFile>
  <PropertyGroup Name="ComboBoxItem" Type="Node" ID="833e0a33-080b-4b2f-bfa3-d209c3a43478" Version="3.10.0.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000" />
      <ObjectData Name="Node" Tag="15" ctype="GameNodeObjectData">
        <Size X="0.0000" Y="0.0000" />
        <Children>
          <AbstractNodeData Name="text" ActionTag="1373616217" Tag="16" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" RightMargin="-220.0000" TopMargin="-53.0000" TouchEnable="True" LabelText="Fnt Text Label" ctype="TextBMFontObjectData">
            <Size X="220.0000" Y="53.0000" />
            <AnchorPoint />
            <Position />
            <Scale ScaleX="0.7000" ScaleY="0.7000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <LabelBMFontFile_CNB Type="Normal" Path="Roboto_bold.fnt" Plist="" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>