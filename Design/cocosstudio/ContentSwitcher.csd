<GameFile>
  <PropertyGroup Name="ContentSwitcher" Type="Node" ID="fb8e25fd-a69d-442a-971d-b7d81d6d33b6" Version="3.10.0.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000" />
      <ObjectData Name="Node" Tag="13" ctype="GameNodeObjectData">
        <Size X="0.0000" Y="0.0000" />
        <Children>
          <AbstractNodeData Name="flower_1_1" ActionTag="-1520537992" Tag="14" IconVisible="False" LeftMargin="-14.5000" RightMargin="-14.5000" TopMargin="-14.0000" BottomMargin="-14.0000" ctype="SpriteObjectData">
            <Size X="29.0000" Y="28.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="flower_1.png" Plist="" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="flower_2_3" ActionTag="-1643937349" Tag="16" IconVisible="False" LeftMargin="-14.5000" RightMargin="-14.5000" TopMargin="-16.0000" BottomMargin="-16.0000" ctype="SpriteObjectData">
            <Size X="29.0000" Y="32.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="flower_2.png" Plist="" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="snail1_4" ActionTag="857559687" Tag="17" IconVisible="False" LeftMargin="-12.0000" RightMargin="-14.0000" TopMargin="-12.8457" BottomMargin="-12.1543" ctype="SpriteObjectData">
            <Size X="26.0000" Y="25.0000" />
            <AnchorPoint ScaleX="0.4615" ScaleY="0.4862" />
            <Position />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="snail1.png" Plist="" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="snail2_5" ActionTag="-516191816" Tag="18" IconVisible="False" LeftMargin="-11.5000" RightMargin="-11.5000" TopMargin="-10.0000" BottomMargin="-10.0000" ctype="SpriteObjectData">
            <Size X="23.0000" Y="20.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="snail2.png" Plist="" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>