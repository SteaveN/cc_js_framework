<GameFile>
  <PropertyGroup Name="ComboBox" Type="Node" ID="ab459357-b56f-49d0-aee8-21234ad53bbd" Version="3.10.0.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000" />
      <ObjectData Name="Node" ctype="GameNodeObjectData">
        <Size X="0.0000" Y="0.0000" />
        <Children>
          <AbstractNodeData Name="btn" ActionTag="-646981319" Tag="2" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="-125.0000" RightMargin="-125.0000" TopMargin="-31.0000" BottomMargin="-31.0000" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="150" Scale9Height="40" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
            <Size X="250.0000" Y="62.0000" />
            <Children>
              <AbstractNodeData Name="title" ActionTag="1235356150" Tag="3" IconVisible="False" LeftMargin="33.0000" RightMargin="-3.0000" TopMargin="5.5000" BottomMargin="3.5000" LabelText="Fnt Text Label" ctype="TextBMFontObjectData">
                <Size X="220.0000" Y="53.0000" />
                <AnchorPoint ScaleY="0.5000" />
                <Position X="33.0000" Y="30.0000" />
                <Scale ScaleX="0.5000" ScaleY="0.5000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.1320" Y="0.4839" />
                <PreSize X="0.8800" Y="0.8548" />
                <LabelBMFontFile_CNB Type="Normal" Path="Roboto_bold.fnt" Plist="" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <TextColor A="255" R="65" G="65" B="70" />
            <DisabledFileData Type="Normal" Path="BTN_Do.png" Plist="" />
            <PressedFileData Type="Normal" Path="BTN_Do.png" Plist="" />
            <NormalFileData Type="Normal" Path="BTN_Do.png" Plist="" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="list" ActionTag="692297071" Tag="6" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="-120.0000" RightMargin="-120.0000" TopMargin="30.0000" BottomMargin="-230.0000" TouchEnable="True" ClipAble="True" BackColorAlpha="102" ComboBoxIndex="1" ColorAngle="90.0000" LeftEage="127" RightEage="127" TopEage="140" BottomEage="140" Scale9OriginX="-127" Scale9OriginY="-140" Scale9Width="254" Scale9Height="280" IsBounceEnabled="True" ScrollDirectionType="0" DirectionType="Vertical" ctype="ListViewObjectData">
            <Size X="240.0000" Y="200.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="1.0000" />
            <Position Y="-30.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <SingleColor A="255" R="150" G="150" B="255" />
            <FirstColor A="255" R="150" G="150" B="255" />
            <EndColor A="255" R="255" G="255" B="255" />
            <ColorVector ScaleY="1.0000" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>