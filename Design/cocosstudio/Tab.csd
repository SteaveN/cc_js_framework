<GameFile>
  <PropertyGroup Name="Tab" Type="Node" ID="b11c2782-8862-437e-8a17-4db9c1d22bb2" Version="3.10.0.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000" />
      <ObjectData Name="Node" Tag="140" ctype="GameNodeObjectData">
        <Size X="0.0000" Y="0.0000" />
        <Children>
          <AbstractNodeData Name="btn" ActionTag="-1047973764" Tag="141" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="-109.5000" RightMargin="-109.5000" TopMargin="-51.0000" BottomMargin="-51.0000" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="70" RightEage="70" TopEage="51" BottomEage="51" Scale9OriginX="70" Scale9OriginY="51" Scale9Width="79" Scale9Height="1" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
            <Size X="219.0000" Y="102.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <TextColor A="255" R="65" G="65" B="70" />
            <DisabledFileData Type="Normal" Path="tab_unactive.png" Plist="" />
            <PressedFileData Type="Normal" Path="tab_touch.png" Plist="" />
            <NormalFileData Type="Normal" Path="tab_active.png" Plist="" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="txtTitle" ActionTag="-2125961984" Tag="142" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="-110.0000" RightMargin="-110.0000" TopMargin="-26.5000" BottomMargin="-26.5000" LabelText="Fnt Text Label" ctype="TextBMFontObjectData">
            <Size X="220.0000" Y="53.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <LabelBMFontFile_CNB Type="Normal" Path="Roboto_bold.fnt" Plist="" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>