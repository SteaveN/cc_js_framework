<GameFile>
  <PropertyGroup Name="UIDialog" Type="Node" ID="20f1cc88-e10d-4030-9254-ddf86064b462" Version="3.10.0.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000" />
      <ObjectData Name="Node" Tag="57" ctype="GameNodeObjectData">
        <Size X="0.0000" Y="0.0000" />
        <Children>
          <AbstractNodeData Name="background" ActionTag="-1524644246" Tag="58" IconVisible="False" LeftMargin="-350.0000" RightMargin="-350.0000" TopMargin="-213.5000" BottomMargin="-213.5000" Scale9Enable="True" LeftEage="127" RightEage="127" TopEage="140" BottomEage="140" Scale9OriginX="127" Scale9OriginY="140" Scale9Width="133" Scale9Height="147" ctype="ImageViewObjectData">
            <Size X="700.0000" Y="427.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="2_Slt_Ketqua_panel.png" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="cancelBtn" ActionTag="-1151299142" Tag="96" IconVisible="True" LeftMargin="-182.0000" RightMargin="182.0000" TopMargin="141.5000" BottomMargin="-141.5000" StretchWidthEnable="False" StretchHeightEnable="False" InnerActionSpeed="1.0000" CustomSizeEnabled="False" ctype="ProjectNodeObjectData">
            <Size X="0.0000" Y="0.0000" />
            <AnchorPoint />
            <Position X="-182.0000" Y="-141.5000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="BMFButton.csd" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="okBtn" ActionTag="-1444408589" Tag="100" IconVisible="True" LeftMargin="-1.0000" RightMargin="1.0000" TopMargin="141.5000" BottomMargin="-141.5000" StretchWidthEnable="False" StretchHeightEnable="False" InnerActionSpeed="1.0000" CustomSizeEnabled="False" ctype="ProjectNodeObjectData">
            <Size X="0.0000" Y="0.0000" />
            <AnchorPoint />
            <Position X="-1.0000" Y="-141.5000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="BMFButton.csd" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="acceptBtn" ActionTag="897580796" Tag="104" IconVisible="True" LeftMargin="180.0000" RightMargin="-180.0000" TopMargin="141.5000" BottomMargin="-141.5000" StretchWidthEnable="False" StretchHeightEnable="False" InnerActionSpeed="1.0000" CustomSizeEnabled="False" ctype="ProjectNodeObjectData">
            <Size X="0.0000" Y="0.0000" />
            <AnchorPoint />
            <Position X="180.0000" Y="-141.5000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="BMFButton.csd" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="txtTitle" ActionTag="-1151224328" Tag="23" IconVisible="False" LeftMargin="-34.5000" RightMargin="-34.5000" TopMargin="-184.5000" BottomMargin="131.5000" LabelText="Title" ctype="TextBMFontObjectData">
            <Size X="69.0000" Y="53.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position Y="158.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <LabelBMFontFile_CNB Type="Normal" Path="Roboto_bold.fnt" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="txtMessage" ActionTag="1385910700" Tag="24" IconVisible="False" LeftMargin="-153.5000" RightMargin="-155.5000" TopMargin="-63.5000" BottomMargin="10.5000" LabelText="Some message here" ctype="TextBMFontObjectData">
            <Size X="309.0000" Y="53.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="1.0000" Y="37.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <LabelBMFontFile_CNB Type="Normal" Path="Roboto_bold.fnt" Plist="" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>