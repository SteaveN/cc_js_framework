<GameFile>
  <PropertyGroup Name="Toasts" Type="Node" ID="9dc4a843-bdff-4e99-a382-7e1ce282e899" Version="3.10.0.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000" />
      <ObjectData Name="Node" Tag="58" ctype="GameNodeObjectData">
        <Size X="0.0000" Y="0.0000" />
        <Children>
          <AbstractNodeData Name="background" ActionTag="1923956368" Tag="63" IconVisible="False" LeftMargin="-426.0000" RightMargin="-426.0000" TopMargin="-26.5000" BottomMargin="-26.5000" LeftEage="281" RightEage="281" TopEage="17" BottomEage="17" Scale9OriginX="281" Scale9OriginY="17" Scale9Width="290" Scale9Height="19" ctype="ImageViewObjectData">
            <Size X="852.0000" Y="53.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="center_black.png" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="txtMessage" ActionTag="-722191515" Tag="60" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="-124.5000" RightMargin="-124.5000" TopMargin="-26.5000" BottomMargin="-26.5000" LabelText="Fnt Text Label a" ctype="TextBMFontObjectData">
            <Size X="249.0000" Y="53.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <LabelBMFontFile_CNB Type="Normal" Path="Roboto_bold.fnt" Plist="" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>