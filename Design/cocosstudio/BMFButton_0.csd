<GameFile>
  <PropertyGroup Name="BMFButton_0" Type="Node" ID="c90fdcfa-60a4-4068-ad76-cb67b62f4d3d" Version="3.10.0.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000" />
      <ObjectData Name="Node" Tag="6" ctype="GameNodeObjectData">
        <Size X="0.0000" Y="0.0000" />
        <Children>
          <AbstractNodeData Name="btn" ActionTag="-1375661053" Tag="7" IconVisible="False" LeftMargin="-90.0000" RightMargin="-90.0000" TopMargin="-31.0000" BottomMargin="-31.0000" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="150" Scale9Height="40" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
            <Size X="180.0000" Y="62.0000" />
            <Children>
              <AbstractNodeData Name="Image_21" ActionTag="2038935159" Tag="196" IconVisible="False" LeftMargin="-14.5000" RightMargin="165.5000" TopMargin="48.0000" BottomMargin="-14.0000" LeftEage="9" RightEage="9" TopEage="9" BottomEage="9" Scale9OriginX="9" Scale9OriginY="9" Scale9Width="11" Scale9Height="10" ctype="ImageViewObjectData">
                <Size X="29.0000" Y="28.0000" />
                <Children>
                  <AbstractNodeData Name="Image_21_0" ActionTag="-2002126208" Tag="197" IconVisible="False" LeftMargin="-4.5000" RightMargin="4.5000" TopMargin="24.0000" BottomMargin="-24.0000" LeftEage="9" RightEage="9" TopEage="9" BottomEage="9" Scale9OriginX="9" Scale9OriginY="9" Scale9Width="11" Scale9Height="10" ctype="ImageViewObjectData">
                    <Size X="29.0000" Y="28.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="10.0000" Y="-10.0000" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.3448" Y="-0.3571" />
                    <PreSize X="1.0000" Y="1.0000" />
                    <FileData Type="Normal" Path="flower_1.png" Plist="" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="Image_21_1" ActionTag="-1669756611" Tag="198" IconVisible="False" LeftMargin="5.5000" RightMargin="-5.5000" TopMargin="34.0000" BottomMargin="-34.0000" LeftEage="9" RightEage="9" TopEage="9" BottomEage="9" Scale9OriginX="9" Scale9OriginY="9" Scale9Width="11" Scale9Height="10" ctype="ImageViewObjectData">
                    <Size X="29.0000" Y="28.0000" />
                    <Children>
                      <AbstractNodeData Name="Image_21_4" ActionTag="961277155" Tag="201" IconVisible="False" LeftMargin="35.5000" RightMargin="-35.5000" TopMargin="64.0000" BottomMargin="-64.0000" LeftEage="9" RightEage="9" TopEage="9" BottomEage="9" Scale9OriginX="9" Scale9OriginY="9" Scale9Width="11" Scale9Height="10" ctype="ImageViewObjectData">
                        <Size X="29.0000" Y="28.0000" />
                        <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                        <Position X="50.0000" Y="-50.0000" />
                        <Scale ScaleX="1.0000" ScaleY="1.0000" />
                        <CColor A="255" R="255" G="255" B="255" />
                        <PrePosition X="1.7241" Y="-1.7857" />
                        <PreSize X="1.0000" Y="1.0000" />
                        <FileData Type="Normal" Path="flower_1.png" Plist="" />
                      </AbstractNodeData>
                      <AbstractNodeData Name="Image_21_5" ActionTag="1450980179" Tag="202" IconVisible="False" LeftMargin="45.5000" RightMargin="-45.5000" TopMargin="74.0000" BottomMargin="-74.0000" LeftEage="9" RightEage="9" TopEage="9" BottomEage="9" Scale9OriginX="9" Scale9OriginY="9" Scale9Width="11" Scale9Height="10" ctype="ImageViewObjectData">
                        <Size X="29.0000" Y="28.0000" />
                        <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                        <Position X="60.0000" Y="-60.0000" />
                        <Scale ScaleX="1.0000" ScaleY="1.0000" />
                        <CColor A="255" R="255" G="255" B="255" />
                        <PrePosition X="2.0690" Y="-2.1429" />
                        <PreSize X="1.0000" Y="1.0000" />
                        <FileData Type="Normal" Path="flower_1.png" Plist="" />
                      </AbstractNodeData>
                      <AbstractNodeData Name="Image_21_6" ActionTag="1229280563" Tag="203" IconVisible="False" LeftMargin="55.5000" RightMargin="-55.5000" TopMargin="84.0000" BottomMargin="-84.0000" LeftEage="9" RightEage="9" TopEage="9" BottomEage="9" Scale9OriginX="9" Scale9OriginY="9" Scale9Width="11" Scale9Height="10" ctype="ImageViewObjectData">
                        <Size X="29.0000" Y="28.0000" />
                        <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                        <Position X="70.0000" Y="-70.0000" />
                        <Scale ScaleX="1.0000" ScaleY="1.0000" />
                        <CColor A="255" R="255" G="255" B="255" />
                        <PrePosition X="2.4138" Y="-2.5000" />
                        <PreSize X="1.0000" Y="1.0000" />
                        <FileData Type="Normal" Path="flower_1.png" Plist="" />
                      </AbstractNodeData>
                    </Children>
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="20.0000" Y="-20.0000" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.6897" Y="-0.7143" />
                    <PreSize X="1.0000" Y="1.0000" />
                    <FileData Type="Normal" Path="flower_1.png" Plist="" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="Image_21_2" ActionTag="1577190469" Tag="199" IconVisible="False" LeftMargin="15.5000" RightMargin="-15.5000" TopMargin="44.0000" BottomMargin="-44.0000" LeftEage="9" RightEage="9" TopEage="9" BottomEage="9" Scale9OriginX="9" Scale9OriginY="9" Scale9Width="11" Scale9Height="10" ctype="ImageViewObjectData">
                    <Size X="29.0000" Y="28.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="30.0000" Y="-30.0000" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="1.0345" Y="-1.0714" />
                    <PreSize X="1.0000" Y="1.0000" />
                    <FileData Type="Normal" Path="flower_1.png" Plist="" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="Image_21_3" ActionTag="1176825413" Tag="200" IconVisible="False" LeftMargin="25.5000" RightMargin="-25.5000" TopMargin="54.0000" BottomMargin="-54.0000" LeftEage="9" RightEage="9" TopEage="9" BottomEage="9" Scale9OriginX="9" Scale9OriginY="9" Scale9Width="11" Scale9Height="10" ctype="ImageViewObjectData">
                    <Size X="29.0000" Y="28.0000" />
                    <Children>
                      <AbstractNodeData Name="Image_21_12" ActionTag="-447896829" Tag="209" IconVisible="False" LeftMargin="115.5000" RightMargin="-115.5000" TopMargin="144.0000" BottomMargin="-144.0000" LeftEage="9" RightEage="9" TopEage="9" BottomEage="9" Scale9OriginX="9" Scale9OriginY="9" Scale9Width="11" Scale9Height="10" ctype="ImageViewObjectData">
                        <Size X="29.0000" Y="28.0000" />
                        <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                        <Position X="130.0000" Y="-130.0000" />
                        <Scale ScaleX="1.0000" ScaleY="1.0000" />
                        <CColor A="255" R="255" G="255" B="255" />
                        <PrePosition X="4.4828" Y="-4.6429" />
                        <PreSize X="1.0000" Y="1.0000" />
                        <FileData Type="Normal" Path="flower_1.png" Plist="" />
                      </AbstractNodeData>
                      <AbstractNodeData Name="Image_21_11" ActionTag="-52201932" Tag="208" IconVisible="False" LeftMargin="105.5000" RightMargin="-105.5000" TopMargin="134.0000" BottomMargin="-134.0000" LeftEage="9" RightEage="9" TopEage="9" BottomEage="9" Scale9OriginX="9" Scale9OriginY="9" Scale9Width="11" Scale9Height="10" ctype="ImageViewObjectData">
                        <Size X="29.0000" Y="28.0000" />
                        <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                        <Position X="120.0000" Y="-120.0000" />
                        <Scale ScaleX="1.0000" ScaleY="1.0000" />
                        <CColor A="255" R="255" G="255" B="255" />
                        <PrePosition X="4.1379" Y="-4.2857" />
                        <PreSize X="1.0000" Y="1.0000" />
                        <FileData Type="Normal" Path="flower_1.png" Plist="" />
                      </AbstractNodeData>
                      <AbstractNodeData Name="Image_21_10" ActionTag="1822045763" Tag="207" IconVisible="False" LeftMargin="95.5000" RightMargin="-95.5000" TopMargin="124.0000" BottomMargin="-124.0000" LeftEage="9" RightEage="9" TopEage="9" BottomEage="9" Scale9OriginX="9" Scale9OriginY="9" Scale9Width="11" Scale9Height="10" ctype="ImageViewObjectData">
                        <Size X="29.0000" Y="28.0000" />
                        <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                        <Position X="110.0000" Y="-110.0000" />
                        <Scale ScaleX="1.0000" ScaleY="1.0000" />
                        <CColor A="255" R="255" G="255" B="255" />
                        <PrePosition X="3.7931" Y="-3.9286" />
                        <PreSize X="1.0000" Y="1.0000" />
                        <FileData Type="Normal" Path="flower_1.png" Plist="" />
                      </AbstractNodeData>
                      <AbstractNodeData Name="Image_21_9" ActionTag="-816148626" Tag="206" IconVisible="False" LeftMargin="85.5000" RightMargin="-85.5000" TopMargin="114.0000" BottomMargin="-114.0000" LeftEage="9" RightEage="9" TopEage="9" BottomEage="9" Scale9OriginX="9" Scale9OriginY="9" Scale9Width="11" Scale9Height="10" ctype="ImageViewObjectData">
                        <Size X="29.0000" Y="28.0000" />
                        <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                        <Position X="100.0000" Y="-100.0000" />
                        <Scale ScaleX="1.0000" ScaleY="1.0000" />
                        <CColor A="255" R="255" G="255" B="255" />
                        <PrePosition X="3.4483" Y="-3.5714" />
                        <PreSize X="1.0000" Y="1.0000" />
                        <FileData Type="Normal" Path="flower_1.png" Plist="" />
                      </AbstractNodeData>
                      <AbstractNodeData Name="Image_21_8" ActionTag="1982012682" Tag="205" IconVisible="False" LeftMargin="75.5000" RightMargin="-75.5000" TopMargin="104.0000" BottomMargin="-104.0000" LeftEage="9" RightEage="9" TopEage="9" BottomEage="9" Scale9OriginX="9" Scale9OriginY="9" Scale9Width="11" Scale9Height="10" ctype="ImageViewObjectData">
                        <Size X="29.0000" Y="28.0000" />
                        <Children>
                          <AbstractNodeData Name="Image_21_13" ActionTag="589041307" Tag="210" IconVisible="False" LeftMargin="125.5000" RightMargin="-125.5000" TopMargin="154.0000" BottomMargin="-154.0000" LeftEage="9" RightEage="9" TopEage="9" BottomEage="9" Scale9OriginX="9" Scale9OriginY="9" Scale9Width="11" Scale9Height="10" ctype="ImageViewObjectData">
                            <Size X="29.0000" Y="28.0000" />
                            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                            <Position X="140.0000" Y="-140.0000" />
                            <Scale ScaleX="1.0000" ScaleY="1.0000" />
                            <CColor A="255" R="255" G="255" B="255" />
                            <PrePosition X="4.8276" Y="-5.0000" />
                            <PreSize X="1.0000" Y="1.0000" />
                            <FileData Type="Normal" Path="flower_1.png" Plist="" />
                          </AbstractNodeData>
                          <AbstractNodeData Name="Image_21_14" ActionTag="-130733974" Tag="211" IconVisible="False" LeftMargin="135.5000" RightMargin="-135.5000" TopMargin="164.0000" BottomMargin="-164.0000" LeftEage="9" RightEage="9" TopEage="9" BottomEage="9" Scale9OriginX="9" Scale9OriginY="9" Scale9Width="11" Scale9Height="10" ctype="ImageViewObjectData">
                            <Size X="29.0000" Y="28.0000" />
                            <Children>
                              <AbstractNodeData Name="Image_21_16" ActionTag="456778738" Tag="213" IconVisible="False" LeftMargin="155.5000" RightMargin="-155.5000" TopMargin="184.0000" BottomMargin="-184.0000" LeftEage="9" RightEage="9" TopEage="9" BottomEage="9" Scale9OriginX="9" Scale9OriginY="9" Scale9Width="11" Scale9Height="10" ctype="ImageViewObjectData">
                                <Size X="29.0000" Y="28.0000" />
                                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                                <Position X="170.0000" Y="-170.0000" />
                                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                                <CColor A="255" R="255" G="255" B="255" />
                                <PrePosition X="5.8621" Y="-6.0714" />
                                <PreSize X="1.0000" Y="1.0000" />
                                <FileData Type="Normal" Path="flower_1.png" Plist="" />
                              </AbstractNodeData>
                              <AbstractNodeData Name="Image_21_17" ActionTag="-804823741" Tag="214" IconVisible="False" LeftMargin="165.5000" RightMargin="-165.5000" TopMargin="194.0000" BottomMargin="-194.0000" LeftEage="9" RightEage="9" TopEage="9" BottomEage="9" Scale9OriginX="9" Scale9OriginY="9" Scale9Width="11" Scale9Height="10" ctype="ImageViewObjectData">
                                <Size X="29.0000" Y="28.0000" />
                                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                                <Position X="180.0000" Y="-180.0000" />
                                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                                <CColor A="255" R="255" G="255" B="255" />
                                <PrePosition X="6.2069" Y="-6.4286" />
                                <PreSize X="1.0000" Y="1.0000" />
                                <FileData Type="Normal" Path="flower_1.png" Plist="" />
                              </AbstractNodeData>
                              <AbstractNodeData Name="Image_21_18" ActionTag="52926980" Tag="215" IconVisible="False" LeftMargin="175.5000" RightMargin="-175.5000" TopMargin="204.0000" BottomMargin="-204.0000" LeftEage="9" RightEage="9" TopEage="9" BottomEage="9" Scale9OriginX="9" Scale9OriginY="9" Scale9Width="11" Scale9Height="10" ctype="ImageViewObjectData">
                                <Size X="29.0000" Y="28.0000" />
                                <Children>
                                  <AbstractNodeData Name="Image_21_19" ActionTag="-354024327" Tag="216" IconVisible="False" LeftMargin="185.5000" RightMargin="-185.5000" TopMargin="214.0000" BottomMargin="-214.0000" LeftEage="9" RightEage="9" TopEage="9" BottomEage="9" Scale9OriginX="9" Scale9OriginY="9" Scale9Width="11" Scale9Height="10" ctype="ImageViewObjectData">
                                    <Size X="29.0000" Y="28.0000" />
                                    <Children>
                                      <AbstractNodeData Name="Image_21_20" ActionTag="-459574871" Tag="217" IconVisible="False" LeftMargin="195.5000" RightMargin="-195.5000" TopMargin="224.0000" BottomMargin="-224.0000" LeftEage="9" RightEage="9" TopEage="9" BottomEage="9" Scale9OriginX="9" Scale9OriginY="9" Scale9Width="11" Scale9Height="10" ctype="ImageViewObjectData">
                                        <Size X="29.0000" Y="28.0000" />
                                        <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                                        <Position X="210.0000" Y="-210.0000" />
                                        <Scale ScaleX="1.0000" ScaleY="1.0000" />
                                        <CColor A="255" R="255" G="255" B="255" />
                                        <PrePosition X="7.2414" Y="-7.5000" />
                                        <PreSize X="1.0000" Y="1.0000" />
                                        <FileData Type="Normal" Path="flower_1.png" Plist="" />
                                      </AbstractNodeData>
                                    </Children>
                                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                                    <Position X="200.0000" Y="-200.0000" />
                                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                                    <CColor A="255" R="255" G="255" B="255" />
                                    <PrePosition X="6.8966" Y="-7.1429" />
                                    <PreSize X="1.0000" Y="1.0000" />
                                    <FileData Type="Normal" Path="flower_1.png" Plist="" />
                                  </AbstractNodeData>
                                </Children>
                                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                                <Position X="190.0000" Y="-190.0000" />
                                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                                <CColor A="255" R="255" G="255" B="255" />
                                <PrePosition X="6.5517" Y="-6.7857" />
                                <PreSize X="1.0000" Y="1.0000" />
                                <FileData Type="Normal" Path="flower_1.png" Plist="" />
                              </AbstractNodeData>
                            </Children>
                            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                            <Position X="150.0000" Y="-150.0000" />
                            <Scale ScaleX="1.0000" ScaleY="1.0000" />
                            <CColor A="255" R="255" G="255" B="255" />
                            <PrePosition X="5.1724" Y="-5.3571" />
                            <PreSize X="1.0000" Y="1.0000" />
                            <FileData Type="Normal" Path="flower_1.png" Plist="" />
                          </AbstractNodeData>
                          <AbstractNodeData Name="Image_21_15" ActionTag="-1146991832" Tag="212" IconVisible="False" LeftMargin="145.5000" RightMargin="-145.5000" TopMargin="174.0000" BottomMargin="-174.0000" LeftEage="9" RightEage="9" TopEage="9" BottomEage="9" Scale9OriginX="9" Scale9OriginY="9" Scale9Width="11" Scale9Height="10" ctype="ImageViewObjectData">
                            <Size X="29.0000" Y="28.0000" />
                            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                            <Position X="160.0000" Y="-160.0000" />
                            <Scale ScaleX="1.0000" ScaleY="1.0000" />
                            <CColor A="255" R="255" G="255" B="255" />
                            <PrePosition X="5.5172" Y="-5.7143" />
                            <PreSize X="1.0000" Y="1.0000" />
                            <FileData Type="Normal" Path="flower_1.png" Plist="" />
                          </AbstractNodeData>
                        </Children>
                        <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                        <Position X="90.0000" Y="-90.0000" />
                        <Scale ScaleX="1.0000" ScaleY="1.0000" />
                        <CColor A="255" R="255" G="255" B="255" />
                        <PrePosition X="3.1034" Y="-3.2143" />
                        <PreSize X="1.0000" Y="1.0000" />
                        <FileData Type="Normal" Path="flower_1.png" Plist="" />
                      </AbstractNodeData>
                      <AbstractNodeData Name="Image_21_7" ActionTag="-1803239164" Tag="204" IconVisible="False" LeftMargin="65.5000" RightMargin="-65.5000" TopMargin="94.0000" BottomMargin="-94.0000" LeftEage="9" RightEage="9" TopEage="9" BottomEage="9" Scale9OriginX="9" Scale9OriginY="9" Scale9Width="11" Scale9Height="10" ctype="ImageViewObjectData">
                        <Size X="29.0000" Y="28.0000" />
                        <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                        <Position X="80.0000" Y="-80.0000" />
                        <Scale ScaleX="1.0000" ScaleY="1.0000" />
                        <CColor A="255" R="255" G="255" B="255" />
                        <PrePosition X="2.7586" Y="-2.8571" />
                        <PreSize X="1.0000" Y="1.0000" />
                        <FileData Type="Normal" Path="flower_1.png" Plist="" />
                      </AbstractNodeData>
                    </Children>
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="40.0000" Y="-40.0000" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="1.3793" Y="-1.4286" />
                    <PreSize X="1.0000" Y="1.0000" />
                    <FileData Type="Normal" Path="flower_1.png" Plist="" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition />
                <PreSize X="0.1611" Y="0.4516" />
                <FileData Type="Normal" Path="flower_1.png" Plist="" />
              </AbstractNodeData>
              <AbstractNodeData Name="Image_21_21" ActionTag="1160655641" Tag="218" IconVisible="False" LeftMargin="205.5000" RightMargin="-54.5000" TopMargin="268.0000" BottomMargin="-234.0000" LeftEage="9" RightEage="9" TopEage="9" BottomEage="9" Scale9OriginX="9" Scale9OriginY="9" Scale9Width="11" Scale9Height="10" ctype="ImageViewObjectData">
                <Size X="29.0000" Y="28.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="220.0000" Y="-220.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="1.2222" Y="-3.5484" />
                <PreSize X="0.1611" Y="0.4516" />
                <FileData Type="Normal" Path="flower_1.png" Plist="" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <TextColor A="255" R="65" G="65" B="70" />
            <DisabledFileData Type="Default" Path="Default/Button_Disable.png" Plist="" />
            <PressedFileData Type="Normal" Path="BTN_Do.png" Plist="" />
            <NormalFileData Type="Normal" Path="BTN_Do.png" Plist="" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="txtTitle" ActionTag="-1860684202" Tag="8" IconVisible="False" LeftMargin="-87.5000" RightMargin="-87.5000" TopMargin="-26.5000" BottomMargin="-26.5000" LabelText="BMFButton" ctype="TextBMFontObjectData">
            <Size X="175.0000" Y="53.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <LabelBMFontFile_CNB Type="Normal" Path="Roboto_bold.fnt" Plist="" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>